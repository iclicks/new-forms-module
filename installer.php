<?php 
require "admin.php";
get_capability('comment');
$title = "Installeer Forms module";
include("admin_header.php");


if (is_admin()){
	
	if(isset($_GET['install'])){
	 
	 mkdir(ABSPATH.UPLOAD_DIR.'attachments');
	 
	 $forms_table = $db->create_table(
	  "CREATE TABLE IF NOT EXISTS `forms` (
		  `id` int(11) NOT NULL auto_increment,
		  `name` varchar(255) NOT NULL,
		  `message` text NOT NULL,
		  `button_value` varchar(255) NOT NULL,
		  `mail` varchar(255) NOT NULL,
		  `captcha` int(8) NOT NULL,
		  `template` varchar(255) NOT NULL,
		  `thankpage` int(8) NULL DEFAULT NULL,
		  `akismet` VARCHAR( 100 ) NULL DEFAULT NULL,
		  PRIMARY KEY  (`id`)
		) ");

	
	$form_values_table = $db->create_table("
		CREATE TABLE IF NOT EXISTS `forms_values` (
		  `id` int(11) NOT NULL auto_increment,
		  `name` varchar(255) NOT NULL,
		  `type` varchar(255) NOT NULL,
		  `mandatory` varchar(255) NOT NULL,
		  `active` varchar(255) NOT NULL,
		  `value` varchar(255) NOT NULL,
		  `order` int(4) NOT NULL,
		  `inline` int(4) NOT NULL,
		  `options` text default NULL,
		  `form_id` int(4) NOT NULL,
		  PRIMARY KEY  (`id`)
		) ");


	 module_installed('forms');
	 
	}
}
include "admin_footer.php";
?>