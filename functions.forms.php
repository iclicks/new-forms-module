<?php

function forms_load_textdomain(){
	load_module_textdomain( 'forms', 'forms/language');
}

function get_form_templates(){
	global $newsletter;
	
	$templates = file_list( FORMS_DIR . '/templates' );
	
	return $templates;
	
}

function get_form_types(){
	
	$form_types = array(
		"text" => 'Tekstveld',
		"email" => 'E-mailveld',
		"textarea" => 'Tekstvlak',
		"heading" => 'Titel',
		"dropdown" => 'Dropdown',
		"checkbox" => 'Checkbox',
		"phone" => 'Telefoonnummer',
		"date" => 'Datum',
		"year" => 'Jaar',
		"time" => 'Tijd',
		"radio" => 'Radio',
		"file" => 'Bestand',
		"copy" => 'Kopie sturen',
		"line" => 'Regel'
		);

	return $form_types;
	
}

function js_form_vars(){
	
	$output = "<script type=\"text/javascript\">\n\r";
	$output .= "var FORMS_URL = '".FORMS_URL."';\n\r";
	$output .= "var HTML_FORM_FIELD_TYPES =\"";
	
	foreach( get_form_types() as $type=>$name){
		$output .= "<option value='".$type."' >".$name."</option>";
	}
	
	$output .= "\";</script>\n\r";
	
	return $output;
}

//returns all forms
function get_forms(){
	global $db;
	$output = '';
	
	$forms = $db->get_results("SELECT * FROM forms");
	$i=0;
	
	if($forms != null){
		
		foreach($forms as $form){
			
			if($i%2){
				$output .= '<tr class="odd">';	
			} else {
				$output .= '<tr class="even">';		
			}
			$output .= '<td>'.$form['id'].'</td>';
			$output .= '<td>'.$form['name'].'</td>';
			$output .= '<td><a href="module_loader.php?module=forms/admin/edit_forms.php&amp;edit_form='.$form['id'].'">
							<img src="'.FORMS_URL.'/images/email_edit.png"  alt="Formulier bewerken" title="'.__('Edit').'">
						</a></td>';
			$output .= '<td><a href="module_loader.php?module=forms/admin/forms.php&amp;copy_form='.$form['id'].'">
							<img src="/admin/images/icons/page_copy.png"  alt="Formulier kopieren" title="'.__('Copy').'">
						</a></td>';
			$output .= '<td><a href="#" onClick="Verwijderpage(\'module_loader.php?module=forms/admin/forms.php&delete_form='.$form['id'].'\')">
							<img src="images/icons/cancel.png"  alt="Formulier verwijderen" title="'.__('Delete').'">
						</a></td>';
			$output .= '</tr>';
			$i++;
		}
		
	} else {
		$output .= '<tr><td colspan="3">Er zijn nog geen formulieren</td></tr>';	
	}
	
	return $output;
	
}

function get_pages_options($id = false) {
	global $db;
	
	$pages = $db->get_results("SELECT * FROM content WHERE parent=0 and type= 'page' AND status IN (0,1) ORDER BY id");
	
	$tree = ""; 
	$depth = 1;
	
	foreach($pages as $page){
		
		if($id == $page['id']){
		
			$tree .= "<option selected='selected' value='".$page['id']."'>".$page['menutitle']."</option>";
			
		} else {
		
		$tree .= "<option value='".$page['id']."'>".$page['menutitle']."</option>";  
		
		}
		
		
		$tree .= build_child_page( $page['id'], $depth, $id, false );        // Start the recursive function of building the child tree
		
	}
	return $tree;
}

function get_form_fields( $form_id ){
	global $db,$local;
	
	$form = $db->get_results("SELECT * FROM forms_values WHERE form_id='$form_id' ORDER BY `order`;");
	$output = '';
	$validator = array();
	if(isset($_POST['form_id']) && $_POST['form_id'] == $form_id) {
		$validator = validate_form_fields($form_id, false);
	}
	if(empty($form)) return false;
	foreach($form as $form_field){
		$form_field_name = generate_clean_name($form_field['name'])."-".$form_field['id'];
		$error = (isset($validator[$form_field_name]) ? ' error-form' : '');
		
		if($form_field['mandatory'] == 1){
			$arterisk = "<span class='form-arterisk'>*</span>";
			$class = "class='form-input$error'";
		} else {
			$arterisk = '';
			$class= 'class="form-input'.$error.'"';
		}
		


		//check for value
		if(isset($_GET[strtolower($form_field['name'])])){
			
			$value = $_GET[strtolower($form_field['name'])];
			
		} else if(isset($_POST) && submit_form(false, $form_id) !== true){
			
			$value = _post($form_field_name);
			
		} else if($form_field['value'] != ''){
			
			$value = $form_field['value'];
			
		} else {
			
			$value = '';
			
		}
		
		
		switch($form_field['type']){
			case 'textarea':
			
				$output .= "<div class='form-row' id='$form_field_name'>";
				
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col8'><textarea $class name='$form_field_name'>$value</textarea></div>\n";	
				} else {
					$output .= "<div class='col-4'>".$form_field['name']."</div>";
					$output .= "<div class='col-4-3'><textarea $class name='$form_field_name'>$value</textarea></div>\n";
				}
				
				$output .= "</div>\n";
				
			break;
			case 'text':
			
				$output .= "<div class='form-row' id='$form_field_name'>";
				
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col8'><input $class value='$value' type='text' name='$form_field_name'/></div>\n";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12'><input $class value='$value' type='text' name='$form_field_name'/></div>\n";
				}
				
				$output .= "</div>\n";
				
			break;
			case 'date':
				$output .= "<div class='form-row' id='$form_field_name'>";
				
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";	
					$output .= "<div class='col8'>";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12'>";	
				}
				
				//day 
				$output .= "<select name='".$form_field_name."-day' class='day col4 form-date-select$error'>\n";
				$output .= "<option value=''>Dag</option>\n";
				for($day=1; $day<32; $day++){
				  $output .= "<option value=\"".$day."\" ".(isset($_POST[$form_field_name.'-day']) && $_POST[$form_field_name.'-day'] == $day ? 'selected="selected"' : '').">".$day."</option>\n";
				}
				$output .= "</select>";
				
				//month
				$output .= "<select name='".$form_field_name."-month' class='month col4 form-date-select$error'>\n";
				$output .= "<option value=''>Maand</option>\n";
				foreach($local->month as $num=>$abbrev){
				  $output .= "<option value=\"".$num."\" ".(isset($_POST[$form_field_name.'-month']) && $_POST[$form_field_name.'-month'] == $num ? 'selected="selected"' : '').">".ucfirst($abbrev)."</option>\n";
				}
				$output .= "</select>";
				
				//year
				if(strpos($form_field['options'],"-")){
					list($start_year, $max_year) = explode("-",$form_field['options']);
					$max_year = ($max_year == 'now') ? date('Y')+1 : $max_year;
				} else {
					$start_year = date('Y');
					$max_year = $start_year + 5;
				}
				
				$output .= "<select name='".$form_field_name."-year' class='year col4 form-date-select$error'>\n";
				$output .= "<option value=''>Jaar</option>\n";
				for($year=$start_year; $year<$max_year; $year++){
				  $output .= "<option value=\"".$year."\" ".(isset($_POST[$form_field_name.'-year']) && $_POST[$form_field_name.'-year'] == $year ? 'selected="selected"' : '').">".$year."</option>\n";
				}
				$output .= "</select>";
                            
				$output .= "</div></div>\n";
			
			break;
			case 'year':
				
				$output .= "<div class='form-row' id='$form_field_name'>";
				
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";	
					$output .= "<div class='col8'>";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12'>";	
				}
				
				
				//year
				if(strpos($form_field['options'],"-")){
					list($start_year, $max_year) = explode("-",$form_field['options']);
					$max_year = ($max_year == 'now') ? date('Y') : $max_year;
				} else {
					$start_year = date('Y');
					$max_year = $start_year + 5;
				}
				
				$output .= "<select name='".$form_field_name."-year' class='year-only form-date-select$error'>\n";
				$output .= "<option value=''>Jaar</option>\n";
				for($year=$max_year; $year>$start_year; $year--){
				  $output .= "<option value=\"".$year."\" ".(isset($_POST[$form_field_name.'-year']) && $_POST[$form_field_name.'-year'] == $year ? 'selected="selected"' : '').">".$year."</option>\n";
				}
				$output .= "</select>";
                            
				$output .= "</div></div>\n";
			
			break;
			case 'time':
			
				$output .= "<div class='form-row' id='$form_field_name'>";

				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";	
					$output .= "<div class='col8'>";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12'>";	
				}
				
				
				//hour
				$output .= "<select name='".$form_field_name."-hour' class='hour col6 form-date-select$error'>\n";
				$output .= "<option value=''>Uur</option>\n";
				for($hour=1; $hour<25; $hour++){
				  $output .= "<option value=\"".$hour."\" ".(isset($_POST[$form_field_name.'-hour']) && $_POST[$form_field_name.'-hour'] == $hour ? 'selected="selected"' : '').">".str_pad($hour, 2, "0", STR_PAD_LEFT)."</option>\n";
				}
				$output .= "</select>";
				
				//minute
				$output .= "<select name='".$form_field_name."-minute' class='minute col6 form-date-select$error'>\n";
				$output .= "<option value=''>Minuten</option>\n";
				for($minute=1; $minute<61; $minute++){
				  $output .= "<option value=\"".$minute."\" ".(isset($_POST[$form_field_name.'-minute']) && $_POST[$form_field_name.'-minute'] == $minute ? 'selected="selected"' : '').">".$minute."</option>\n";
				}
				$output .= "</select>";
				
                            
				$output .= "</div></div>\n";
			
			break;
			case 'email':
			
				$output .= "<div class='form-row' id='$form_field_name'>";
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col8'><input $class value='$value' type='text' name='$form_field_name'/></div>\n";	
				} else {
					$output .= "<div class='col12'>".$form_field['name']."</div>";
					$output .= "<div class='col12'><input $class value='$value' type='text' name='$form_field_name'/></div>\n";
				}
				$output .= "</div>";
			
			break;
			case 'copy':
				$output .= "<div class='form-row' id='$form_field_name'>";
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";	
					$output .= "<div class='col8'>";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12'>";	
				}
				$output .= "<input id='copy_field' $class  type='checkbox' name='$form_field_name'/>";
				$output .= "</div></div>";
			
			break;
			case 'phone':

				$output .= "<div class='form-row' id='$form_field_name'>";
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";	
					$output .= "<div class='col8'>";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12'>";	
				}
				$output .= "<input id='form_phone_field' $class value='$value' type='text' name='$form_field_name'/>";
				$output .= "</div></div>";

			
			
			break;
			case 'checkbox':
			
				$output .= "<div class='form-row' id='$form_field_name'>";
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";	
					$output .= "<div class='col8$error'>";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12$error'>";	
				}

				$checkboxes = explode(",",$form_field['options']);
				$checkbox_count = 0;
				
				foreach($checkboxes as $checkbox){
					
					if($value == $checkbox){
						$output .= "<p><input id='".$form_field_name."-".$checkbox_count."' type='checkbox' checked='checked' value='".$checkbox."' name='".$form_field_name."[]'/><label>".$checkbox."</label></p>\n";
						
					} else {
						
						$output .= "<p><input id='".$form_field_name."-".$checkbox_count."' type='checkbox' value='".$checkbox."' name='".$form_field_name."[]'/><label>".$checkbox."</label></p>\n";
						
					}
					
					$checkbox_count++;
					
				}
				
				$output .= "</div></div>\n";
			
			break;
			case 'heading':

				$output .= "<div class='form-row' id='$form_field_name'>";
				$output .= "<h2>".$form_field['name']."</h2>";
				$output .= "</div>";

			break;
			case 'line':
				$output .= "<div class='form-row fline' id='$form_field_name'>";
				$output .= "<strong>".$form_field['name']."</strong>";
				$output .= "</div>";
				
			break;
			case 'file':
				$output .= "<div class='form-row' id='$form_field_name'>";
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";	
					$output .= "<div class='col8$error'>";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12$error'>";	
				}
				$output .= "<input type='file' class='' name='".$form_field_name."'/></div></div>";
				
			break;
			case 'radio':
				$radioboxes = explode(",",$form_field['options']);
				
				$output .= "<div class='form-row form-row-radio' id='$form_field_name'>";
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";	
					$output .= "<div class='col8$error'>";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12$error'>";	
				}
				
				$count = 1;
				
				foreach($radioboxes as $radiobox){
					
					if($count == 1 || $value == $radiobox){
						$checked = 'checked="checked" ';	
					} else {
						$checked = '';	
					}
					
					$output .= "<input type='radio' value='".$radiobox."' name='$form_field_name' $checked/><label>".$radiobox."</label>";
					
					$count++;
					
				}
				
				$output .= "</div></div>";
			
			break;
			case 'dropdown':
				$dropdown_options = explode(",",$form_field['options']);
				
				$output .= "<div class='form-row' id='$form_field_name'>";
				if($form_field['inline']){
					$output .= "<div class='col4'>".$form_field['name'].$arterisk."</div>";	
					$output .= "<div class='col8'>";	
				} else {
					$output .= "<div class='col12'>".$form_field['name'].$arterisk."</div>";
					$output .= "<div class='col12'>";	
				}
				$output .= "<select name='$form_field_name' class='form-date-select$error'>";
				$output .= "<option value=''>Selecteer</option>\n";
				foreach($dropdown_options as $dropdown_option){
					if($value == strtolower($dropdown_option)){
						$output .= "<option selected='selected' value='".$dropdown_option."'/>".$dropdown_option."</option>\n";
					} else {
						$output .= "<option value='".$dropdown_option."'/>".$dropdown_option."</option>\n";
					}
				}
				$output .= "</select></div>";

				$output .= "</div>";
			break;
			
		}
		

	}


	$output .=  apply_filters('form_fields','');
	
	return $output;	
}

//return form by id
function get_form( $form_id ){
	global $db,$captcha,$page;
	$output = '';
	
	$form_details = $db->get_row("SELECT * FROM forms WHERE id='$form_id'");
	if(empty($form_details)) return false;
	//display thanks message
	if(strlen(submit_form(false, $form_id)) > 1){
		$output .= "<div class='errorbox'><h2>" . submit_form( false, $form_id ) . "</h2></div>\n";
		
	} elseif(submit_form(true, $form_id) == true) {
		// if($form_details->thankpage > 0) {
		// 	header("Location: ".$page->getLink($form_details->thankpage, true)."?mail=ttttt");
		// } else {
			$output .= "<div class='thanks'><div class='thanks_l'><i class='fa fa-check'></i></div><div class='thanks_r'><h2>Uw formulier is succesvol verstuurd!</h2><p>".$form_details->message."</p></div></div>";
		// }
		
	}
	
	$output .= "<div class='requiredMessage'></div>\n";
	$output .= '<form enctype="multipart/form-data" method="post" class="custom_form required-form" action="?mail=send" onsubmit="return validateform(this);">';

			
	//start form
	$output .= "<div class='form' id='form-".$form_id."'>\n";
	
	$output .= custom_edit_link(siteurl().'admin/module_loader.php?module=forms/admin/edit_forms.php&edit_form='.$form_id,'modules/forms/images/email_edit.png');
	
	if($form_details->template && $form_details->template != 'default'){
		$output .= file_get_contents( FORMS_DIR . "/templates/" . $form_details->template );
	} else {
		$output .= get_form_fields($form_id);
	}
	//add captcha
	if($form_details->captcha){
		// $output .= "<tr><td></td><td><img id='captcha-image' src='".siteurl()."form.captcha'/>\n";
		// $output .= "<a id='refresh-captcha' href='#'>Niet leesbaar? Verander tekst</a></td></tr>\n";
		// $output .= "<tr><td></td><td><input type='text' class='required' name='captcha'/></td></tr>\n";
		$output .= "<div class='form-row' id='form_captcha'>";
		$output .= "<div class='col12'>Neem over</div>";
		$output .= "<div class='form-row'>";
		$output .= "<div class='col6'><img id='captcha-image' src='".siteurl()."?the_captcha'/></div>";
		$output .= "<div class='col6'><input type='text' class='form-input required' name='captcha'/>";
		$output .= "<a id='refresh-captcha' href='#'>Niet leesbaar? Verander tekst</a>";
		$output .= "</div></div></div>";
		
	}
	
	$output .= "<div class='form-row'>
					<div class='col12'>
						<input type='hidden' name='form_id' value='$form_id'/><input class='button' id='send_form' name='send_form' value='".$form_details->button_value."' type='submit'/>
					</div>
				</div>\n";
	
	$output .= "</div>\n";
	$output .= "</form>\n";
	
	return $output;
	
}

function checkmails($mail) { 

	$email_host = explode("@", $mail); 
	$email_host = $email_host['1']; 
	$email_resolved = gethostbyname($email_host); 

	if ($email_resolved != $email_host && eregi("^[0-9a-z]([-_.~]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-z]{2,4}$",$mail)) 
		$valid = 1; 

	return $valid; 
} 

//add custom shortcode for module
function forms_shortcode($attr, $content = null){
	
	extract(shortcode_atts(array(
		'id'	=> ''
	), $attr));

	return get_form($id);
}

function validate_form_fields($form_id, $checkonly = true) {
	global $db;
	if(isset($_POST['form_id']) && $_POST['form_id'] == $form_id ){
		$form = $db->get_row("SELECT * FROM forms WHERE id='".$_POST['form_id']."'");
		$form_fields = $db->get_results("SELECT * FROM `forms_values` WHERE `form_id` = '".$_POST['form_id']."' ORDER BY `order`;");
		$ret_arr = array();

		foreach($form_fields as $field) {
			$name = generate_clean_name($field['name'])."-".$field['id'];
			if($field['active'] == 1 && $field['mandatory'] == 1) {
				switch($field['type']) {
					case "text":
						if(isset($_POST[$name])) {
							if(strlen($_POST[$name]) <= 0)
								$ret_arr[$name] = '';
						} else $ret_arr[$name] = '';
					break;
					case "email":
						if (isset($_POST[$name])) {
							if(!filter_var($_POST[$name], FILTER_VALIDATE_EMAIL))
								$ret_arr[$name] = '';
						} else $ret_arr[$name] = '';
					break;
					case "textarea":
						if(isset($_POST[$name])) {
							if(strlen($_POST[$name]) <= 0)
								$ret_arr[$name] = '';
						} else $ret_arr[$name] = '';
					break;
					case "date":
						if(isset($_POST[$name.'-day']) && isset($_POST[$name.'-month']) && isset($_POST[$name.'-year'])) {
							$datum = $_POST[$name.'-day'] . '/' . $_POST[$name.'-month'] . '/' . $_POST[$name.'-year'];
							$dateclass = DateTime::createFromFormat('d/m/Y', $datum);
							$dateclass_errors = DateTime::getLastErrors();
							if ($dateclass_errors['warning_count'] + $dateclass_errors['error_count'] > 0) {
							    $ret_arr[$name] = '';
							}
						} else $ret_arr[$name] = '';
					break;
					case "year":
						if(isset($_POST[$name.'-year'])) {
							if(strpos($field['options'],"-")){
								list($start_year, $max_year) = explode("-",$field['options']);
								$max_year = ($max_year == 'now') ? date('Y') : $max_year;
							} else {
								$start_year = date('Y');
								$max_year = $start_year + 5;
							}
							if($_POST[$name.'-year'] < $start_year || $_POST[$name.'-year'] > $max_year)
								$ret_arr[$name] = '';
						} else $ret_arr[$name] = '';
					break;
					case "time":
						if(isset($_POST[$name.'-hour']) && ctype_digit($_POST[$name.'-hour']) && isset($_POST[$name.'-minute']) && ctype_digit($_POST[$name.'-minute'])) {
							if($_POST[$name.'-hour'] < 0 || $_POST[$name.'-hour'] > 24 || $_POST[$name.'-minute'] < 0 || $_POST[$name.'-minute'] > 60)
								$ret_arr[$name] = '';
						} else $ret_arr[$name] = '';
					break;
					case "radio":
						if(isset($_POST[$name])) {

						} else $ret_arr[$name] = '';
					break;
					case "file":
						if(isset($_FILES[$name]) && $_FILES[$name]['error'] == UPLOAD_ERR_OK) {

						} else $ret_arr[$name] = '';
					break;
					case "checkbox":
						if(isset($_POST[$name])) {

						} else $ret_arr[$name] = '';
					break;
					case "dropdown":
						if(isset($_POST[$name]) && strlen($_POST[$name]) > 0) {
						} else $ret_arr[$name] = '';
					break;
					case "phone":
						if(isset($_POST[$name])) {
							if(!is_numeric($_POST[$name])) $ret_arr[$name] = '';
						} else $ret_arr[$name] = '';
					break;
				}
			}
		}
		if($checkonly == true) {
			return empty($ret_arr);
		} else {
			return $ret_arr;
		}
	}
}

function submit_form( $send = true, $fid ){
	global $db,$message;
	
	$akismet_enabled = false;
	$debug_akismet = false;
	$debug_akismet_email = 'ferhat@iclicks.nl';

	if ($_SERVER['REMOTE_ADDR'] == "46.231.81.112" && $debug_akismet == true && $send == true) {
		echo "<script>alert('Akismet debug is enabled. All mails will be sent to \"".htmlspecialchars($debug_akismet_email, ENT_QUOTES)."\".');</script>";
	}

	//add form
	if(isset($_POST['form_id']) && $_POST['form_id'] == $fid ){
		
		$form = $db->get_row("SELECT * FROM forms WHERE id='".$_POST['form_id']."'");
		$form_fields = $db->get_results("SELECT * FROM `forms_values` WHERE `form_id` = '".$_POST['form_id']."' ORDER BY `order`;");

		// backend validation ferhat
		$can_send = validate_form_fields($fid);
		if(!$can_send) return 'Vul alle velden juist in';
		// backend validation ferhat

		$akismetValues = array();
		if($form->akismet) $akismet_enabled = true;

		if ($_SERVER['REMOTE_ADDR'] == "46.231.81.112" && $debug_akismet == true && $send == true) {
			echo "<script>alert('Akismet status: ".($akismet_enabled == true ? "true" : 'false').".');</script>";
		}

		$date = date("d.m.Y H:i"); 
		$ip = $_SERVER['REMOTE_ADDR']; 
		$send_copy = false;
		$mail_adress = false;
		$files = '';
		
		$output = "<strong>Ingevuld formulier: " . $form->name . "</strong><br/>";
		$output .= "<table>";
		

		foreach($form_fields as $form_field){
			$name = generate_clean_name($form_field['name'])."-".$form_field['id'];
			if($form_field['type'] == 'file'){

				if(isset($_FILES[$name])) {
					$fname = $_FILES[$name]['name'];
					$ftemp = $_FILES[$name]['tmp_name'];
					$target = ABSPATH.UPLOAD_DIR."attachments/".$fname;
					$upload = move_uploaded_file($ftemp, $target);
					$form_field_post = "<a href=\"".siteurl().'uploads/attachments/'.$fname."\">".$fname."</a>";
					$form_field_name = $form_field['name'];
					$files[] = $target;
				} else {
					$form_field_post = "Geen bestand geüpload.";
					$form_field_name = $form_field['name'];
				}

			} else if($form_field['type'] == 'heading'){
				
				$form_field_post = "";
				$form_field_name = "<strong>".$form_field['name']."</strong>";
			
			} else if($form_field['type'] == 'line'){
				
				$form_field_post = "";
				$form_field_name = $form_field['name'];
					
			} else {
				
				if(isset($_POST[generate_clean_name($form_field['name'])."-".$form_field['id']])){
					$form_field_post = $_POST[generate_clean_name($form_field['name'])."-".$form_field['id']];
				}
				$form_field_name = $form_field['name'];
				
			}
			
			//checkbox
			if($form_field['type'] == 'checkbox' && isset($_POST[$name])){
				$form_field_post = implode(" + ",$form_field_post);	
			}
			
			//email
			if($form_field['type'] == 'email'){
				$mail_adress = $form_field_post;
			}
			
			//date
			if($form_field['type'] == 'date'){
				$day = $_POST[generate_clean_name($form_field['name'])."-".$form_field['id'] . '-day'];
				$month = $_POST[generate_clean_name($form_field['name'])."-".$form_field['id'] . '-month'];
				$year = $_POST[generate_clean_name($form_field['name'])."-".$form_field['id'] . '-year'];
				$form_field_post = $day."-".$month."-".$year;
			}
			
			//time
			if($form_field['type'] == 'time'){
				$hour = $_POST[generate_clean_name($form_field['name'])."-".$form_field['id'] . '-hour'];
				$minute= $_POST[generate_clean_name($form_field['name'])."-".$form_field['id'] . '-minute'];
				$form_field_post = $hour.":".$minute;
			}
			
			//copy
			if($form_field['type'] == 'copy' && isset($form_field_post)){
				
				$send_copy = $mail_adress;
			
			} else {
			
				$output .= "<tr><td>".$form_field_name."</td><td>".$form_field_post."</td></tr>";
				
			}

			if ($form_field['type'] == 'textarea') { $akismetValues['body'] = $form_field_post; }

			if ($form_field['type'] == 'email') { $akismetValues['email'] = $form_field_post; }

			if (stristr($form_field['name'], 'naam')) { $akismetValues['author'] = $form_field_post; }
		
		}
		
		$output .= "</table>";
		
		$output .= "<br/>";
		$output .= "Verstuurd op " . $date . " via het ip " . $ip . "\n\n"; 
	
		if($mail_adress){
			$reply = array(
				'name'	=> $mail_adress,
				'mail'	=> $mail_adress
			);	
		} else $reply = false;

		if($akismet_enabled) {
			$akismet = new Akismet(siteurl(), $form->akismet, $akismetValues);
			if ($debug_akismet && $_SERVER['REMOTE_ADDR'] == "46.231.81.112" && $akismet->isSpam()) {
				echo "<script>alert('Akismet has detected spam. (Only visible for iClicks IP)');</script>";
			}
		}

				
		if($send_copy && $send){
			
			if ($akismet_enabled) {
				if((!$akismet->errorsExist() && !$akismet->isSpam()) || $akismet->errorsExist()) {
					send_mail($send_copy,'admin',$output,'Formulier: '.$form->name);
				}
			} else {
				send_mail($send_copy,'admin',$output,'Formulier: '.$form->name);
			}
		}
		
		if ($form->captcha){
			if(isset($_SESSION['captcha']) && isset($_POST['captcha'])) {
				if($_SESSION['captcha'] == $_POST['captcha']) {
					if($akismet_enabled) {
						if((!$akismet->errorsExist() && !$akismet->isSpam()) || $akismet->errorsExist()) {
							if($send) {
								send_mail( $form->mail,'admin',$output,'Formulier: '.$form->name, true, false, $reply, $files);
								$message->add(
								array(
									'title' => 'Formulier: '.$form->name,
									'description' => $output,
									'user_level' => 2
									)
								);
							}
						}
						return true;
					} else {
						if($send){
							send_mail( $form->mail,'admin',$output,'Formulier: '.$form->name, true, false, $reply, $files);
							$message->add(
							array(
								'title' => 'Formulier: '.$form->name,
								'description' => $output,
								'user_level' => 2
								)
							);
						}
						return true;
					}
				}
			}
			return 'Captcha foutief';
		} else {
			if($akismet_enabled) {
				if((!$akismet->errorsExist() && !$akismet->isSpam()) || $akismet->errorsExist()) {
					if($send) {
						send_mail( $form->mail,'admin',$output,'Formulier: '.$form->name, true, false, $reply, $files);
						$message->add(
							array(
								'title' => 'Formulier: '.$form->name,
								'description' => $output,
								'user_level' => 2
							)
						);
					}
				}
				return true;
			} else {
				if($send){
					send_mail( $form->mail,'admin',$output,'Formulier: '.$form->name, true, false, $reply, $files);
					$message->add(
						array(
							'title' => 'Formulier: '.$form->name,
							'description' => $output,
							'user_level' => 2
						)
					);
				}
				return true;
			}
			
		}
		
	} else {
		return false;	
	}
	
}
?>
