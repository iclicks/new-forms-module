<?php
/*
Module Name: Forms module
Module URI: 
Description: Remon Versteeg forms module, Ferhat Yildirim (updates)
Version: 2.2.0.0
Author: Remon Versteeg
Author URI: http://www.remonversteeg.nl
CMS version: 1.4.00
Dependencies: Core
Languages: nl_NL,en_US
*/
// if(!session_id()){
// 	session_start();
// }
//defines
define("FORMS_URL",siteurl()."modules/forms");
define("FORMS_DIR",ROOT."modules/forms");
define("FORMS_RESOURCES",ROOT."modules/forms/resources/");

if(!class_exists('Akismet')) {
   include "classes/class.akismet.php";
}
include "functions.forms.php";
include "classes/class.forms.php";

$forms = new forms();

forms_load_textdomain();

$rewrite->php_file("captcha", FORMS_DIR . "/captcha.php", false);

//add items to admin menu
if(is_module_installed('forms')){
	admin_menu_add(10,'Formulieren','module_loader.php?module=forms/admin/forms.php','2');
} else {
	admin_menu_add(10,'Installeer Formulieren','module_loader.php?module=forms/installer.php&install','1');
}

//css and js
add_admin_script( siteurl().'modules/forms/js/forms_admin.js', 'forms');
add_theme_script( siteurl().'modules/forms/js/forms.js', 'forms');
add_theme_script( siteurl().'modules/forms/css/forms.css', 'forms');


//register wysiwyg button
add_editor_button('forms');

$tinymce_forms = "
      // Register example button
      ed.addButton('forms', {
         title : 'Formulier invoegen',
         image : '".siteurl()."modules/forms/images/form.gif',
         onclick : function() {
         	tb_show('Formulier invoegen', '../modules/forms/tinymce.php?keepThis=true&amp;TB_iframe=true&amp;height=100&amp;width=300');
			tinymce.DOM.setStyle( ['TB_overlay','TB_window','TB_load'], 'z-index', '999999' );
         }
      });
";
add_editor_plugin($tinymce_forms);




add_admin_head( js_form_vars() );


//remove form
if(isset($_POST['remove_form_field']) && $_POST['remove_form_field'] == 'true'){
	
	$db->delete("DELETE FROM forms_values where id ='".$_POST['form_id']."'");
	exit();
	
}

add_shortcode('forms','forms_shortcode');

if(isset($_GET['the_captcha'])) {
   include "captcha_img.php";
}

?>