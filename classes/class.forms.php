<?php

class forms{
	
	
	function __construct(){
		
	}
	
	//add a page
	public function add( $table, $vars ){
		global $db;
		
		foreach($vars as $key=>$value){
			$keys[] = $key;
			$values[] = $value;
		}
	
		$insert_page = $db->insert("INSERT INTO `".$db->escape($table)."` ( ".implode_addslashes($keys, ',', '`' )." ) VALUES ( ".implode_addslashes($values)." )");
		
		return $db->insert_id();
		
	}
	
	public function update( $id, $vars ){
		die('unused function');
		// global $db;
		
		// $vars = implode_with_key($vars);
		// $update_page = $db->update("UPDATE products SET $vars WHERE id='$id'"); 
		
		// return $update_page;
	}
	
	public function duplicate( $id ){
		global $db;
		
		$form = $db->get_row("SELECT * FROM forms WHERE id='".$db->escape($id)."';");
		if(!empty($form)) {
			$new_form = $this->add('forms', array(
				'name' => $db->escape($form->name . " - Kopie"),
				'message' => $db->escape($form->message),
				'button_value' => $db->escape($form->button_value),
				'mail' => $db->escape($form->mail),
				'captcha' => $db->escape($form->captcha),
				'template' => $db->escape($form->template),
				'thankpage' => $db->escape($form->thankpage),
			));
			$vals = $db->get_results("SELECT * FROM forms_values WHERE form_id='".$db->escape($id)."';");
			if(!empty($vals)) {
				foreach($vals as $row) {
					$db->insert("INSERT INTO forms_values (`name`, `type`, `mandatory`, `active`, `order`, `options`, `value`, `inline`, `form_id`) VALUES ('".$db->escape($row['name'])."', '".$db->escape($row['type'])."', '".$db->escape($row['mandatory'])."', '".$db->escape($row['active'])."', '".$db->escape($row['order'])."', '".$db->escape($row['options'])."', '".$db->escape($row['value'])."', '".$db->escape($row['inline'])."', '".$new_form."')");
				}
			}
			return true;
		}
		return false;
	}
	
	public function delete( $id ){
		global $db;

		$form = $db->get_row("SELECT * FROM forms WHERE id='".$db->escape($id)."';");
		if(!empty($form)) {
			$db->delete("DELETE FROM forms WHERE id='".$id."'");
			$db->delete("DELETE FROM forms_values WHERE form_id='".$id."'");
			return true;
		}
		return false;
	}
	
	public function get( $id ){
		global $db;
		
		$page_obj = $db->get_row("SELECT * FROM forms where id = '$id'");
		
		return $page_obj;
	}
	
	
}