<?php

class CaptchaFerhat {

	////////////////////////
	// Config, changeable //============
	////////////////////////
	public $wordcount = '1';
	public $resources_path = FORMS_RESOURCES;
	public $image_width = '330';
	public $image_height = '100';
	public $font_size = '20';
	public $word_list = 'words.txt';
	public $circle_count = '10';
	public $max_word_length = '16';
	public $background_color = array(255, 255, 255);
	public $shadow_color = array(0, 0, 0);
	public $max_rotation = 15;
	////////////////////////
	// Config, changeable //============
	////////////////////////

	// Background image (not needed)
	public $bgimg = '';
	public $fgimg = '';
	public $txtshadow_col;

	// Fonts
	public $fonts = array(
		'Antykwa'  => array('spacing' => -3, 'minSize' => 27, 'maxSize' => 30, 'font' => 'AntykwaBold.ttf'),
		'Candice'  => array('spacing' =>-1.5,'minSize' => 28, 'maxSize' => 31, 'font' => 'Candice.ttf'),
		'DingDong' => array('spacing' => -2, 'minSize' => 24, 'maxSize' => 30, 'font' => 'Ding-DongDaddyO.ttf'),
		'Duality'  => array('spacing' => -2, 'minSize' => 30, 'maxSize' => 38, 'font' => 'Duality.ttf'),
		'Heineken' => array('spacing' => -2, 'minSize' => 24, 'maxSize' => 34, 'font' => 'Heineken.ttf'),
		'Jura'     => array('spacing' => -2, 'minSize' => 28, 'maxSize' => 32, 'font' => 'Jura.ttf'),
		'StayPuft' => array('spacing' =>-1.5,'minSize' => 28, 'maxSize' => 32, 'font' => 'StayPuft.ttf'),
		'Times'    => array('spacing' => -2, 'minSize' => 28, 'maxSize' => 34, 'font' => 'TimesNewRomanBold.ttf'),
		'VeraSans' => array('spacing' => -1, 'minSize' => 20, 'maxSize' => 28, 'font' => 'VeraSansBold.ttf'),
		'AHG'      => array('spacing' => -1, 'minSize' => 20, 'maxSize' => 28, 'font' => 'AHGBold.ttf'),
	);

	// Wave config
	public $y_period    = 2;
	public $y_amplitude = 2;
	public $x_period    = 2;
	public $x_amplitude = 2;


	// Backgroundimage scale (quality, higher = better)
	public $scale = 2;

	// Image formatting
	public $image_format = 'png';
	// GD image
	public $img;

	public function displayImage() {
		ob_start();
		$captcha_text = $this->generateImage();
		if ($this->image_format == 'png') {
			header("Content-type: image/png");
			imagepng($this->img);
		} else {
			header("Content-type: image/jpeg");
			imagejpeg($this->img, null, 80);
		}
		$this->wipeImage();
		return $captcha_text;
	}

	public function generateImage() {
		$this->initializeImage();
		$captcha_text = $this->generateText();
		$font = $this->fonts[array_rand($this->fonts)];
		$this->setBackground();
		$this->drawCircles();
		$this->writeText($captcha_text, $font);
		$this->waveImage();
		$this->reduceImage();
		return $captcha_text;
	}

	public function initializeImage() {
		if (!empty($this->img)) {
			imagedestroy($this->img);
		}
		$this->img = imagecreatetruecolor($this->image_width*$this->scale, $this->image_height*$this->scale);
		$this->bg_col = imagecolorallocate($this->img,
			$this->background_color[0],
			$this->background_color[1],
			$this->background_color[2]
		);
		imagefilledrectangle($this->img, 0, 0, $this->image_width*$this->scale, $this->image_height*$this->scale, $this->bg_col);
		$this->txt_col = imagecolorallocate($this->img, rand(0, 255), rand(0, 255), rand(0, 255)); // random  color
		if (!empty($this->shadowColor) && is_array($this->shadowColor) && sizeof($this->shadowColor) >= 3) {
            $this->txtshadow_col = imagecolorallocate($this->img,
                $this->shadowColor[0],
                $this->shadowColor[1],
                $this->shadowColor[2]
            );
        }
	}

	protected function waveImage() {
	    $xp = $this->scale*$this->x_period*rand(1,3);
	    $k = rand(0, 100);
	    for ($i = 0; $i < ($this->image_width*$this->scale); $i++) {
	        imagecopy($this->img, $this->img,
	            $i-1, sin($k+$i/$xp) * ($this->scale*$this->x_amplitude),
	            $i, 0, 1, $this->image_height*$this->scale);
	    }
	    $k = rand(0, 100);
	    $yp = $this->scale*$this->y_period*rand(1,2);
	    for ($i = 0; $i < ($this->image_height*$this->scale); $i++) {
	        imagecopy($this->img, $this->img,
	            sin($k+$i/$yp) * ($this->scale*$this->y_amplitude), $i-1,
	            0, $i, $this->image_width*$this->scale, 1);
	    }
	}

	protected function reduceImage() {
	    $image_resampled = imagecreatetruecolor($this->image_width, $this->image_height);
	    imagecopyresampled($image_resampled, $this->img,
	        0, 0, 0, 0,
	        $this->image_width, $this->image_height,
	        $this->image_width*$this->scale, $this->image_height*$this->scale
	    );
	    imagedestroy($this->img);
	    $this->img = $image_resampled;
	}

	public function setBackground() {
		$bgdir = $this->resources_path . 'backgrounds/';
		if ($this->bgimg == '') {
			if (is_dir($bgdir) && is_readable($bgdir)) {
				$img = $this->getBackgroundFromDirectory();
				if ($img != false) {
					$this->bgimg = $img;
				}
			}
		}

		if ($this->bgimg == '') {
			return;
		}

		$dat = @getimagesize($this->bgimg);
		if($dat == false) {
			return;
		}

		switch($dat[2]) {
			case 1:  $newim = @imagecreatefromgif($this->bgimg); break;
			case 2:  $newim = @imagecreatefromjpeg($this->bgimg); break;
			case 3:  $newim = @imagecreatefrompng($this->bgimg); break;
			default: return;
		}
		if(!$newim) return;
		imagecopyresized($this->img, $newim, 0, 0, 0, 0,$this->image_width*2, $this->image_height*2,imagesx($newim), imagesy($newim));
	}

	protected function getBackgroundFromDirectory()
	{
		$images = array();

		if ( ($dh = opendir(($this->resources_path . 'backgrounds/'))) !== false) {
			while (($file = readdir($dh)) !== false) {
				if (preg_match('/(jpg|gif|png)$/i', $file)) $images[] = $file;
			}

			closedir($dh);

			if (sizeof($images) > 0) {
				return rtrim(($this->resources_path . 'backgrounds/'), '/') . '/' . $images[mt_rand(0, sizeof($images)-1)];
			}
		}

		return false;
	}

	protected function WriteText($text, $fonts = array()) {
		if (empty($fonts)) {
			$fonts  = $this->fonts[array_rand($this->fonts)];
		}
		$fontfile = $this->resources_path.'/fonts/'.$fonts['font'];
		$lettersMissing = $this->max_word_length-strlen($text);
		$fontSizefactor = 1+($lettersMissing*0.09);
		$x      = 20*$this->scale;
		$y      = round(($this->image_height*27/40)*$this->scale);
		$length = strlen($text);
		for ($i=0; $i<$length; $i++) {
			$degree   = rand($this->max_rotation*-1, $this->max_rotation);
			$fontsize = rand($fonts['minSize'], $fonts['maxSize'])*$this->scale*$fontSizefactor;
			$letter   = substr($text, $i, 1);
			if ($this->shadow_color) {
                $coords = imagettftext($this->img, $fontsize, $degree,
                	$x+$this->scale, $y+$this->scale,
                    $this->txtshadow_col, $fontfile, $letter);
            }
			$coords = imagettftext($this->img, $fontsize, $degree,
				$x, $y,
				$this->txt_col, $fontfile, $letter);
			$x += ($coords[2]-$x) + ($fonts['spacing']);
		}
		$this->textFinalX = $x;
	}

	public function generateText() {
		$words = explode(PHP_EOL, file_get_contents($this->resources_path . $this->word_list));

		shuffle($words); 
		$captcha_string = trim($words[rand(0, sizeof($words)-1)]); 
		if(is_numeric($this->wordcount) && $this->wordcount > 1) {
			for($i = 0; $i < $this->wordcount; $i++) {
				$captcha_string .= " " . trim($words[rand(0, sizeof($words)-1)]);
			}
		}
		return $captcha_string;
	}

	public function drawCircles() {
		for ($i = 0; $i < $this->circle_count; $i++) {
			$cx = (int) rand(-1*($this->image_width/2), $this->image_width + ($this->image_width/2));
			$cy = (int) rand(-1*($this->image_height/2), $this->image_height + ($this->image_height/2));
			$h  = (int) rand($this->image_height/2, 2*$this->image_height);
			$w  = (int) rand($this->image_width/2, 2*$this->image_width);
			imageellipse($this->img, $cx, $cy, $w, $h, imagecolorallocate($this->img, rand(0, 255), rand(0, 255), rand(0, 255)));
		}
	}

	public function wipeImage() {
		imagedestroy($this->img);
	}
}