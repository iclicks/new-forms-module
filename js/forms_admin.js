function add_form_field() {
  time = new Date();
  new_element_number = time.getTime();
  new_element_id = "form_id_"+new_element_number;
  
  new_element_contents = "";
  //new_element_contents += "<tr>\n\r";
  new_element_contents += "<td class='namecol'><input type='text' name='new_form_name["+new_element_number+"]' value='' /></td>\n\r";
  new_element_contents += "<td class='typecol'><select class='inputsmall' name='new_form_type["+new_element_number+"]'>"+HTML_FORM_FIELD_TYPES+"</select></td>\n\r"; 
  new_element_contents += "<td class='optionscol'><textarea name='new_form_options["+new_element_number+"]'></textarea></td>\n\r";
  new_element_contents += "<td class='infocol'><img src='/admin/images/icons/information.png'></td>\n\r";
  new_element_contents += "<td class='mandatorycol'><input type='checkbox' name='new_form_mandatory["+new_element_number+"]' value='1' /></td>\n\r";
  new_element_contents += "<td class='inlinecol'><input type='checkbox' name='new_form_inline["+new_element_number+"]' value='1' /></td>\n\r";
  new_element_contents += "<td class='ordercol'><input type='text' size='3' class='inputsmall' name='new_form_order["+new_element_number+"]' value='' /></td>\n\r";
  new_element_contents += "<td  style='text-align: center; width: 12px;'><a class='image_link' href='#' onclick='return remove_new_form_field(\""+new_element_id+"\");'><img src='"+FORMS_URL+"/images/bin_closed.png' alt='verwijder' title='Verwijder' /></a></td>\n\r";
 // new_element_contents += "<td></td>\n\r";
 // new_element_contents += "</tr>";

  new_element = document.createElement('tr');
  new_element.id = new_element_id;
  new_element.className = 'even';
   
  document.getElementById("form_field_form_container").appendChild(new_element);
  document.getElementById(new_element_id).innerHTML = new_element_contents;
 
  return false;
}

function remove_new_form_field(id) {
  element_count = document.getElementById("form_field_form_container").childNodes.length;
  if(element_count > 1) {
    target_element = document.getElementById(id);
    document.getElementById("form_field_form_container").removeChild(target_element);
  }
  return false;
}
  
function remove_form_field(id,form_id) {
  var delete_variation_value=function(results) { };
  element_count = document.getElementById("form_field_form_container").childNodes.length;
  if(element_count > 1) {
    $.post("index.php",{remove_form_field: "true", form_id: form_id });
	$('#'+id).remove();
   // target_element = document.getElementById(id);
    //document.getElementById("form_field_form_container").removeChild(target_element);
	
  }
  
  return false;
}