/*
iMod forms module
copyright RemonVersteeg - www.remonversteeg.nl
*/

//checks if required forms are filled
function validateform(form){
	
	var message = '';
	var cont = true;
	
	$(form).find('input.required').each(function() {
		var singleValues = jQuery(this).val();
		
		if(singleValues == ''){
			jQuery(this).css('border','1px solid red');
			message += jQuery(this).attr('name')+' is leeg<br/>';

			cont = false;
		} else {
			jQuery(this).css('border','1px solid #CCCCCC');
		}
		
		if(jQuery(this).attr('id') == 'email_field'){
		
			if(validateEmail(jQuery(this).val()) == false){
				cont = false;
			 	message += 'Ongeldig e-mail adres.<br/>';
			}
		
		}
		
		if(jQuery(this).attr('id') == 'phone_field'){
		
			if(validatePhone(jQuery(this).val()) == false){
				cont = false;
			 	message += 'Ongeldig telefoonnummer.<br/>';
			}
		
		}
		
	});
	
  if(cont == true){
	  jQuery('.requiredMessage').html();
	return true;
  } else {
	 //jQuery('.requiredMessage').html(message); 
	return false;  
  }
  
}

//update if field isn't empty anymore
jQuery(document).ready(function(){
								
	$('#refresh-captcha').click(function( event ){
		event.preventDefault();
		src = $('#captcha-image').attr('src');
		$('#captcha-image').attr('src', src);				   
	});

	jQuery(':input.required').keyup(function() {
	
		jQuery(this).css('border','1px solid #CCCCCC');
	
	});
	
	jQuery('.form input[type=text]').focus(function(){
		this.value = '';	
	});
							 
});

//validates email adres
function validateEmail(email){
	
	var emailPattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	return emailPattern.test(email);

}

//validates if is phone_number
function validatePhone(phone) {
	
	//phone.replace(/[\(\)\.\-\ ]/g, ''); 
	var phonePattern = /^[0-9\(\)\-\+]/;   

   return phonePattern.test(phone);
    

}