<?php
require "admin.php";
$title= __('Forms','forms');
include("admin_header.php");

global $db;
?>

<div class="optionBanner" ><div class="stBtn"><a href='module_loader.php?module=forms/admin/edit_forms.php' class='add_item_link'><?=__('Add Form','forms');?></a></div></div>

<?php

if(isset($_GET['delete_form'])){
	if($forms->delete( $_GET['delete_form'] )) {
		$_SESSION['flash_msg'] = "<div class='message message_succes'>Formulier ".$_GET['delete_form']." is met success verwijderd</div>";
	}
	header("Location: /admin/module_loader.php?module={$_GET['module']}");
}

if(isset($_GET['copy_form'])){
	if($forms->duplicate( $_GET['copy_form'] )) {
		$_SESSION['flash_msg'] = "<div class='message message_succes'>Formulier ".$_GET['copy_form']." is met success gekopiëerd</div>";
	}
	header("Location: /admin/module_loader.php?module={$_GET['module']}");
}

?>
	<?php if(!isset($_GET['copy_form']) && !isset($_GET['delete_form']) && isset($_SESSION['flash_msg'])) {
		echo $_SESSION['flash_msg'];
		unset($_SESSION['flash_msg']);
	} ?>
    <table class="table table-advanced zebra" cellspacing="0">
    <thead><th style="width:60px">ID</th><th><?=__('Forms','forms');?></th><th style="width:60px"><?=__('Edit','forms');?></th><th style="width:60px"><?=__('Copy')?></th><th style="width:60px"><?=__('Delete','forms');?></th></thead>
        <?=get_forms();?>
    </table>
    
    
<?php include "admin_footer.php";?>