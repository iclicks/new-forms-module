<?php
require "admin.php";
$title= __('Edit form','forms');
include("admin_header.php");

global $db;
?>

<form method='post' id='forms_table'>
<div class="optionBanner" ><div class="stBtn"><a href='module_loader.php?module=forms/admin/edit_forms.php' class='add_item_link'><?=__('Add Form')?></a><input type='submit' name='submit' class='button-primary right' value='<?=__('Save');?>' /></div></div>

<?php

if(isset($_POST['forms'])){
	
	
	$forms = $db->escape($_POST['forms']);
	$message = $db->escape($_POST['message']);
	$mail = $db->escape($_POST['mail']);
	$button_value = $db->escape($_POST['button_value']);
	$captcha = $db->escape((isset($_POST['captcha']) ? $_POST['captcha'] : '0'));
	$template = $db->escape($_POST['template']);
	$akismet = $db->escape($_POST['akismet']);
	$thankpage = $db->escape((isset($_POST['thankpage']) ? $_POST['thankpage'] : '0'));
	
	if(!isset($_GET['edit_form'])){
		
		$db->insert("INSERT INTO `forms` (`name`,`message`,`button_value`,`mail`,`captcha`,`template`,`akismet`,`thankpage`) VALUES ( '$forms','$message','$button_value','$mail','$captcha','$template','$akismet','$thankpage');");
		$forms_id = $db->insert_id;
	
	} else {
		
		$db->update("UPDATE `forms` SET `name` = '$forms',`message` = '$message', `button_value` = '$button_value',`mail` = '$mail',`captcha` = '$captcha',`template` = '$template' ,`akismet` = '$akismet' ,`thankpage` = '$thankpage' WHERE id='".$_GET['edit_form']."';");
		$forms_id = $_GET['edit_form'];
	
	}


	if(isset($_POST['new_form_name'])){
		
		foreach($_POST['new_form_name'] as $form_id => $form_name){
		
			$form_type = $_POST['new_form_type'][$form_id];
			$options = $_POST['new_form_options'][$form_id];
			$form_mandatory = 0;
			if( isset($_POST['new_form_mandatory'][$form_id]) && $_POST['new_form_mandatory'][$form_id] == 1) {  $form_mandatory = 1;  }
			$form_inline = 0;
			if( isset($_POST['new_form_inline'][$form_id]) && $_POST['new_form_inline'][$form_id] == 1) {  $form_inline = 1;  }
			$max_order_sql = "SELECT MAX(`order`) AS `order` FROM `forms_values` WHERE `active` = '1';";
			
			if($_POST['new_form_order'][$form_id] != ''){
				$order_number = $_POST['new_form_order'][$form_id];
			} else {
				$max_order_sql = $db->get_results($max_order_sql);
				$order_number = $max_order_sql[0]['order'] + 1;
			}
			
			$db->insert("INSERT INTO `forms_values` ( `name`, `type`, `mandatory`, `inline`,  `active`, `order`, `options`, `form_id` ) VALUES ( '$form_name', '$form_type', '$form_mandatory','$form_inline',  '1','".$order_number."','".$options."','".$forms_id."');");

		}
		
	} 
		
		if(isset($_POST['form_name']) && $_POST['form_name'] != null){
			
			foreach($_POST['form_name'] as $form_id => $form_name){
				
				$form_type = $_POST['form_type'][$form_id];
				$options = $_POST['form_options'][$form_id];
				$form_mandatory = 0;
				$form_inline = 0;
				if( isset($_POST['form_mandatory'][$form_id]) && $_POST['form_mandatory'][$form_id] == 1) {  $form_mandatory = 1;  }
				if( isset($_POST['form_inline'][$form_id]) && $_POST['form_inline'][$form_id] == 1) {  $form_inline = 1;  }
				$form_order = $_POST['form_order'][$form_id];
				
				$db->update("UPDATE `forms_values` SET `name` = '$form_name', `type` = '$form_type', `mandatory` = '$form_mandatory',`inline` = '$form_inline', `options` = '$options',  `order` = '$form_order' WHERE `id` ='".$form_id."' LIMIT 1 ;");
			
			}
			
		}
		echo "<div class='message message_succes'>Formulier is opgeslagen!</div>";
	}



?>


        <table class="table table-advanced zebra" cellspacing="0" id="form_field_form_container">
        	<thead><tr><th colspan="8"><?=__('Options', 'forms');?></th></tr></thead>
            <tbody>
            <?php 
			if(isset($_GET['edit_form'])){
				
				$get_form = $db->get_row("SELECT * FROM forms WHERE id='".$_GET['edit_form']."'");
				$output = "<tr class='odd'><td>".__('Form name', 'forms')."</td><td colspan='7'><input type='text' name='forms' value='".$get_form->name."'/></td></tr>";
				$output .= "<tr class='odd'><td>".__('Submit message', 'forms')."</td><td colspan='7'><input type='text' name='message' value='".$get_form->message."'/></td></tr>";
				$output .= "<tr class='odd'><td>".__('Email adress')."</td><td colspan='7'><input type='text' name='mail' value='".$get_form->mail."'/></td></tr>";
				$output .= "<tr class='odd'><td>".__('Button text', 'forms')."</td><td colspan='7'><input type='text' name='button_value' value='".$get_form->button_value."'/></td></tr>";
				
				$output .= "<tr>
						<td>Template</td>
						<td colspan='7'>
							<select name='template'>
								<option value='default'>".__('Default')."</option>";
								
									foreach(get_form_templates() as $template){
										$selected = ($get_form->template == $template) ? "selected='selected'" : '';
										$output .= "<option value='".$template."' $selected>".$template."</option>";
									}
							
				$output .= "			</select>
						</td>
					</tr>";

					$output .= "<tr class='odd'><td>Akismet API key</td><td colspan='7'><input type='text' name='akismet' value='".$get_form->akismet."'></td>";
					$output .= "<tr class='odd'><td>Bedankpagina</td><td colspan='7'><select name='thankpage'><option value='0'>Geen</option>";
					$output .= get_pages_options($get_form->thankpage);
					$output .= "</select></td></tr>";
            
				//captcha
				$output .= "<tr class='odd'><td>iMod Captcha</td><td colspan='7'>";
				$yesorno = array(1 => __('Yes'),0 => __('No'));
                foreach ( $yesorno as $key => $value) {
                    $selected = ($get_form->captcha == $key) ? 'checked="checked"' : '';
                    $output .= "\n\t<input type='radio' name='captcha' value='$key' $selected/><label>$value</label>\n";
                }
				$output .= "</td></tr>";
				
				$output .= "<tr class='table-title'><th>".__('Name', 'forms')."</th><th>".__('Type', 'forms')."</th><th>".__('Options', 'forms')."</th><th></th><th>".__('Required','forms')."</th><th>".__('Inline','Forms')."</th><th>".__('Order')."</th><th>".__('Delete')."</th></tr>";
				
				$forms = $db->get_results("SELECT * FROM forms_values WHERE form_id = '".$_GET['edit_form']."' ORDER BY `order`;");
				
				if($forms != null){
					foreach($forms as $form_field){
						
						$output .= "<tr id=\"form_id_".$form_field['id']."\">\n";
						$output .= "<td class='namecol'><input type='text' name='form_name[".$form_field['id']."]' value='".$form_field['name']."' /></td>\n";
						
						$output .= "<td class='typecol'><select class='inputsmall' name='form_type[".$form_field['id']."]'>\n";
						
						foreach(get_form_types() as $type=>$name) {
						  $selected = ($type == $form_field['type']) ? "selected='selected'" : '';
						  $output .= "<option value='".$type."' ".$selected.">".$name."</option>\n";
						  
						}
						
						$output .= "</select></td>\n";
						
						
						$checked_mandatory = ($form_field['mandatory'])? "checked='true'" : "";
						$checked_inline = ($form_field['inline'])? "checked='true'" : "";
		
						$output .= "<td class='optionscol'><textarea name='form_options[".$form_field['id']."]'>".$form_field['options']."</textarea></td>\n";
						$output .= "<td class='infocol' style=\"width:10px;\"><img src='".siteurl()."admin/images/icons/information.png'></td>\n";
						
						$output .= "<td class='mandatorycol'><input $checked_mandatory type='checkbox' name='form_mandatory[".$form_field['id']."]' value='1' /></td>\n";
						
						$output .= "<td class='inlinecol'><input $checked_inline type='checkbox' name='form_inline[".$form_field['id']."]' value='1' /></td>\n";
						
						$output .= "<td class='ordercol'><input type='text' size='3' class='inputsmall' name='form_order[".$form_field['id']."]' value='".$form_field['order']."' /></td>\n";
						
						$output .= "<td style='text-align: center; width: 12px;'><a class='image_link' href='#' onclick='return remove_form_field(\"form_id_".$form_field['id']."\",".$form_field['id'].");'><img src='".FORMS_URL."/images/bin_closed.png' alt='".__('Delete')."' title='".__('Delete')."' /></a></td>\n";
						$output .= "</tr>\n";
						
					}
				
				}
				
			} else {
				
				$output = "<tr class='odd'><td>".__('Form name', 'forms')."</td><td colspan='7'><input type='text' name='forms' value=''/></td></tr>\n";
				$output .= "<tr class='odd'><td>".__('Submit message', 'forms')."</td><td colspan='7'><input type='text' name='message' value=''/></td></tr>\n";
				$output .= "<tr class='odd'><td>".__('Email adress')."</td><td colspan='7'><input type='text' name='mail' value='".get_option('emailadres')."'/></td></tr>";
				$output .= "<tr class='odd'><td>".__('Button text', 'forms')."</td><td colspan='7'><input type='text' name='button_value' value='".__('Send')."'/></td></tr>";
				$output .= "<tr>
						<td>Template</td>
						<td colspan='7'>
							<select name='template'>
								<option value='default'>".__('Default')."</option>";
								
									foreach(get_form_templates() as $template){
										$output .= "<option value='".$template."'>".$template."</option>";
									}
							
				$output .= "			</select>
						</td>
					</tr>";
				$output .= "<tr class='odd'><td>Akismet API key</td><td colspan='7'><input type='text' name='akismet' value=''></td>";
				$output .= "<tr class='odd'><td>Bedankpagina</td><td colspan='7'><select name='thankpage'><option value='0'>Geen</option>";
				$output .= get_pages_options();
				$output .= "</select></td></tr>";
				//captcha
				$output .= "<tr class='odd'><td>iMod Captcha</td><td colspan='7'>";
				$yesorno = array(1 => __('Yes'),0 => __('No'));
                foreach ( $yesorno as $key => $value) {
                    $output .= "\n\t<input type='radio' name='captcha' value='$key'/><label>$value</label>\n";
                }
				$output .= "</td></tr>";
				
				$output .= "<tr class='table-title'><th>".__('Name', 'forms')."</th><th>".__('Type', 'forms')."</th><th>".__('Options', 'forms')."</th><th></th><th>".__('Required','forms')."</th><th>".__('Inline','Forms')."</th><th>".__('Order')."</th><th>".__('Delete')."</th></tr>";
				
			}
			
			echo $output;
					
			?>

           </tbody>
        </table>
        
            <input type='submit' name='submit' class='button-primary right' value='<?=__('Save');?>' />
            <input type="button" onclick='return add_form_field();' value="<?=__('Add new field', 'forms');?>"/>
        </form>
<script>
	$(document).ready(function() {
		var text = [];
		text['text'] = "Simpele tekstveld, dit heeft geen speciale opties.";
		text['email'] = "TODO";
		text['date'] = "Voer het als volgt in: 'yyyy-yyyy' (bijv: '2000-2017')<br/> Ook kun je 'now' in vullen om de huidige jaar te gebruiken (bijv: '1990-now')";
		text['year'] = "Voer het als volgt in: 'yyyy-yyyy' (bijv: '2000-2017')<br/> Ook kun je 'now' in vullen om de huidige jaar te gebruiken (bijv: '1990-now')";
		text['checkbox'] = "Voer de waardes in gescheiden met een komma.<br/>(bijv: 'appel,peer,banaan')";
		text['radio'] = "Voer de waardes in gescheiden met een komma.<br/>(bijv: 'appel,peer,banaan')";
		$(document).on('mouseenter', ".infocol", function(e) {//
			var $type = $(this).parent().find(".typecol select[name^='form_type'] option:selected").val();
			if(text[$type]) {
				$("body").append("<p id='tooltip' class='preview_box'>"+ text[$type] +"</p>");
				$("#tooltip").css("top",(e.pageY - xOffset) + "px").css("left",(e.pageX + yOffset) + "px").fadeIn("fast");
			}
		});
		$(document).on('mousemove', ".infocol", function(e) {
			if($("#tooltip").length)
				$("#tooltip").css("top",(e.pageY - xOffset) + "px").css("left",(e.pageX + yOffset) + "px");
		});
		$(document).on('mouseleave', ".infocol", function(e) {
			if($("#tooltip").length)
				$("#tooltip").remove();
		});
	});
</script>
<?php
    include "admin_footer.php";
?>