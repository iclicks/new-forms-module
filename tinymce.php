<?php

include ($_SERVER['DOCUMENT_ROOT']."/admin/admin.php");

global $db;

if(isset($_POST['get_form_id'])){

	send_to_editor(insert_shortcode('forms',array('id' => $_POST['get_form_id'])));
	
} else {

	$forms = $db->get_results("SELECT * FROM forms");
	
	
	$output = "<form method='post'>";
	
	$output .= "<select name='get_form_id'>";
	
	foreach($forms as $form){
		$output .= "<option value='".$form['id']."'>".$form['name']."</option>";
	}
	
	$output .= "</select>";
	
	$output .= "<input type='submit' name='insert_form' value='".__('Insert')."'/>";
	
	$output .= "</form>";

	echo $output;
	
}

?>